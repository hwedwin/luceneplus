package org.wltea.analyzer.lucene;

import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;

import com.ld.zxw.config.LucenePlusConfig;

/**
 * IK分词器，Lucene Analyzer接口实现
 * 兼容Lucene 6.x版本
 */
public final class IKAnalyzer extends Analyzer{
	
	private boolean useSmart;
	
	private LucenePlusConfig lucenePlusConfig;
	
	public boolean useSmart() {
		return useSmart;
	}

	public void setUseSmart(boolean useSmart) {
		this.useSmart = useSmart;
	}

	/**
	 * IK分词器Lucene  Analyzer接口实现类
	 * 
	 * 默认细粒度切分算法
	 */
	public IKAnalyzer(){
		this(false);
	}
	
	/**
	 * IK分词器Lucene Analyzer接口实现类
	 * 
	 * @param useSmart 当为true时，分词器进行智能切分
	 */
	public IKAnalyzer(boolean useSmart){
		super();
		this.useSmart = useSmart;
	}
	public IKAnalyzer(boolean useSmart,LucenePlusConfig lucenePlusConfig){
		this.useSmart = false;
		this.lucenePlusConfig = lucenePlusConfig;
	}

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {
		Reader reader=new StringReader(fieldName);
		IKTokenizer ikTokenizer = new IKTokenizer(reader, this.useSmart,lucenePlusConfig);
		return new TokenStreamComponents(ikTokenizer);
	}
}
