package com.ld.zxw.core;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.NoLockFactory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ld.zxw.config.LuceneDataSource;
import com.ld.zxw.config.LucenePlusConfig;

public class LucenePlus {
	private Logger log = Logger.getLogger(LucenePlugin.class);

	public List<Map<String, LucenePlusConfig>> list;


	/**
	 * 不使用动态词库
	 */
	public LucenePlus() {
		list = Lists.newArrayList();
	}

	/**
	 * 加载redis  使用动态词库
	 * @param redisHost 地址：例 127.0.0.1 必填
	 * @param redisPort 端口号： 例 6379  必填
	 * @param redisPwd  密码 ： 不必填
	 */
//	public LucenePlus(String redisHost,int redisPort,String redisPwd) {
//		list = Lists.newArrayList();
//		LuceneDataSource.build().jedis = new Jedis(redisHost,redisPort == 0 ? 6379 : redisPort);
//		if(redisPwd != null){
//			LuceneDataSource.build().jedis.auth(redisPwd);
//		}
//		LuceneDataSource.build().DynamicDictionary = true;
//	}

	/**
	 * 添加数据源
	 * @param <T>
	 * @param LucenePlusConfig
	 * @param sourceName
	 * @param analyzer
	 * @param sourceKey
	 * @param class1 
	 */
	public void putDataSource(LucenePlusConfig lucenePlusConfig,String sourceKey) {
		Map<String, LucenePlusConfig> map = Maps.newHashMap();
		map.put(sourceKey, lucenePlusConfig);
		list.add(map);
	}


	public boolean start() {
		try {
			int size = list.size();
			for (int i = 0; i < size; i++) {
				Map<String, LucenePlusConfig> map = list.get(i);
				
				for (Entry<String, LucenePlusConfig> entry : map.entrySet()) {
					String key = entry.getKey();
					LucenePlusConfig config = entry.getValue();
					if(config.getAnalyzer() == null) {
						config.setAnalyzer(new PerFieldAnalyzerWrapper(new IKAnalyzer(false,config)));
					}
					
					LucenePlusConfig plusConfig = new LucenePlusConfig();
					plusConfig.setAnalyzer(config.getAnalyzer());
					plusConfig.setLucenePath(config.getLucenePath());
					plusConfig.setHighlight(config.isHighlight());
					plusConfig.setHighlightConf(config.getHighlightConf());
					plusConfig.setBoostField(config.getBoostField());
					plusConfig.setParticipleField(config.getParticipleField());
					plusConfig.setHighlightFields(config.getHighlightFields());
					setPath(key,plusConfig);
					//预定义读写对象
					setIndexWriter(plusConfig);
					setIndexSearcher(plusConfig);
					LuceneDataSource.build().dataSource.put(key,plusConfig);
				}
			}
			log.info("LucenePlus 加载完毕!");
			return true;
		} catch (Exception e) {
			log.error("LucenePlugin.start()", e);
			return false;
		}
	}

	/**
	 * 设置搜索对象
	 * @param LucenePlusConfig
	 */
	public void setIndexSearcher(LucenePlusConfig LucenePlusConfig) {
		DirectoryReader open = null;
		try {
			open = DirectoryReader.open(LucenePlusConfig.getIndexWriter());
		} catch (IOException e) {
			e.printStackTrace();
		}
		IndexSearcher searcher = new IndexSearcher(open);
		LucenePlusConfig.setIndexSearcher(searcher);
	}

	/**
	 * 设置写入对象
	 * @param LucenePlusConfig
	 */
	public void setIndexWriter(LucenePlusConfig LucenePlusConfig) {
		IndexWriter indexWriter = null;
		try {
			indexWriter = new IndexWriter(FSDirectory.open(LucenePlusConfig.getPath(), NoLockFactory.INSTANCE),
					new IndexWriterConfig(LucenePlusConfig.getAnalyzer()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		LucenePlusConfig.setIndexWriter(indexWriter);
	}

	/**
	 * 设置目录
	 * @param conf
	 * @param sourceKey
	 */
	public void setPath(String sourceKey,LucenePlusConfig conf) {
		try {
			String lucene_core = conf.getLucenePath()+sourceKey+"/data";
			//不存在 创建
			File file = new File(lucene_core);
			if(!file.isDirectory()) {
				file.mkdirs();
			}
			Path path = Paths.get(lucene_core);
			conf.setPath(path);
		} catch (Exception e) {
			log.error("LucenePlugin.setPath()", e);
		}
	}

	public boolean stop() {
		Map<String, LucenePlusConfig> dataSource = LuceneDataSource.build().dataSource;
		for (Map.Entry<String, LucenePlusConfig> entry : dataSource.entrySet()) {  
			log.info("关闭索引源:"+entry.getKey());
			close(entry.getValue().getIndexWriter(),true);
		}  
		return true;
	}
	
	/**
	 * 提交数据
	 * @return
	 */
	public boolean commit() {
		Map<String, LucenePlusConfig> dataSource = LuceneDataSource.build().dataSource;
		for (Map.Entry<String, LucenePlusConfig> entry : dataSource.entrySet()) {  
			close(entry.getValue().getIndexWriter(),false);
		}  
		return true;
	}
	public static boolean commiT() {
		Map<String, LucenePlusConfig> dataSource = LuceneDataSource.build().dataSource;
		for (Map.Entry<String, LucenePlusConfig> entry : dataSource.entrySet()) {  
			System.out.println("手动提交数据源:"+entry.getKey());
			LucenePlus plus = new LucenePlus();
			plus.close(entry.getValue().getIndexWriter(),false);
		}  
		return true;
	}
	
	private void close(IndexWriter writer,boolean flag){
		try {
			writer.commit();
		} catch (Exception e) {
			try {
				writer.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			if(flag){
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
