                            
# 本项目改名为LuceneX 最新地址:[LuceneX](https://gitee.com/Myzhang/LuceneX)
### LucenePlus  让全文检索更简单，更快捷，更容易维护  & 作者QQ：523812162 『欢迎提出意见』<br/>


### 简介
    lucenePlus 是基于 lucene 6.x 的薄封装，具有 高性能、 易学易用、极其稳定、内置功能丰富 的全文搜索框架
中文手册：链接：http://pan.baidu.com/s/1eRDR4m2 密码：r637
### 下载
    版本发布地址:[版本](https://gitee.com/Myzhang/luceneplus/releases)
### 优势
    。百万数据3秒必达
    。实时检索
    。入门简单
    。性能极高
### 特点
    。实时检索
    。多数据源设计
    。无缝升级lucene
    。强大的本地词库设计
    。自带中文分词器
    。高亮一键设置
    。一键增删改查
    。可定义动态评分
    。自动回收资源
### 应用场景
    论坛、商城、音乐、电影、小说 等。。。。 各个领域